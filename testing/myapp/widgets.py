from django.forms.widgets import MultiWidget
from django import forms
from django.db.models import get_model
from django.utils.safestring import mark_safe


class FKAutoComplete(MultiWidget):
    model = None
    query_field = None
    query_url = "/fk-search"
    script_tpl = """
        <script>
            $(document).ready(function(){
                FKAutoComplete.register({
                    input: $("#%(id2)s"),
                    hidden: $("#%(id1)s"),
                    model: "%(model)s",
                    url: "%(query_url)s",
                    queryField: "%(query_field)s",
                });
            });
        </script>
    """
    add_tpl = """
        <button type="button" onclick="window.open('%(url)s', '_blank')">Adicionar</button>
    """
    add_url = None

    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop("model")
        self.query_field = kwargs.pop("query_field")
        self.add_url = kwargs.pop("add_url", None)
        if kwargs.get("query_url", None):
            self.query_url = kwargs.pop("query_url")

        widgets = [
            forms.HiddenInput(),
            forms.TextInput(),
            ]

        super(FKAutoComplete, self).__init__(widgets, *args, **kwargs)

    def decompress(self, value):
        if value:
            model_class = get_model(*self.model.split("."))
            obj = model_class.objects.get(pk=value)
            return [obj.pk, "%s - %s" % (obj.pk, unicode(obj))]
        return [None, None]

    def render(self, name, value, attrs=None):
        ret = super(FKAutoComplete, self).render(name, value, attrs)
        script = self.script_tpl % {
            "id1": attrs["id"] + "_0",
            "id2": attrs["id"] + "_1",
            "model": self.model,
            "query_url": self.query_url,
            "query_field": self.query_field,
        }
        ret += script

        if self.add_url:
            ret += self.add_tpl % {
                "url": self.add_url,
                }

        return mark_safe(ret)

    def value_from_datadict(self, data, files, name):
        value = super(FKAutoComplete, self).value_from_datadict(data, files, name)
        if value and value[0] and value[0].isdigit():
            return value[0]
        return None

