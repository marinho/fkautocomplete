FKAutoComplete = function(options){
    this.input = options.input;
    this.hidden = options.hidden;
    this.model = options.model;
    this.url = options.url;
    this.queryField = options.queryField;
    this.initialize();
}

FKAutoComplete.prototype.initialize = function(){
    _this = this;

    _this.input.autocomplete({
      minLength: 1,
      source: function( request, response ) {
        var data = {
          model: _this.model,
          queryField: _this.queryField,
          q: request.term,
        }

        $.ajax({
          url: _this.url,
          dataType: "jsonp",
          data: data,
          success: function( data ) {
            response( $.map( data.objects, function( item ) {
              return {
                label: item.label,
                value: item.label,
                id: item.id
              }
            }));
          }
        });
      },
      change: function( event, ui ) {
          if ( !ui.item ) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i" ), valid = false;

            _this.input.children("option").each(function() {
              if ($(this).text().match(matcher)) {
                this.selected = valid = true;
                return false;
              }
            });

            if (!valid) {
              // remove invalid value, as it didn't match anything
              $(this).val("");
              _this.input.val("");
              _this.hidden.val("");
              _this.input.autocomplete("search", "");
              return false;
            }
          }
      },
      select: function( event, ui ) {
        _this.hidden.val(ui.item.id);
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
}

FKAutoComplete.register = function(options){
    var widget = new FKAutoComplete(options);
    options.input.data("widget", widget);
}


