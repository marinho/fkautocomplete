from django.forms import ModelForm
from widgets import FKAutoComplete
from models import Entrada


class MyForm(ModelForm):
    class Meta:
        model = Entrada

    def __init__(self, *args, **kwargs):
        super(MyForm, self).__init__(*args, **kwargs)
        self.fields["user"].widget = FKAutoComplete(
                model="auth.User",
                query_field="username__icontains",
                add_url="/pessoa/aditionar/",
                )
