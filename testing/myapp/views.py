import json
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.db.models import get_model

from forms import MyForm
from models import Entrada


def home(request, pk=None):
    if pk:
        entry = get_object_or_404(Entrada, pk=pk)
    else:
        entry = None

    if request.method == "POST":
        form = MyForm(request.POST, instance=entry)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/")
    else:
        form = MyForm(instance=entry)

    return render(request, "myapp/home.html", {
        "form": form,
        })


ALLOWED_MODELS = ("auth.User",)


def fk_search(request):
    callback = request.GET.get("callback", "callback")
    order_by = request.GET.get("orderBy", "")

    try:
        model = request.GET["model"]
        query_field = request.GET["queryField"]
        q = request.GET["q"]
    except KeyError:
        return HttpResponseBadRequest()

    ret = {
        "count": 0,
        "objects": [],
    }

    if model in ALLOWED_MODELS:
        model_class = get_model(*model.split("."))

        objects = model_class.objects.all()
        if order_by:
            objects = objects.filter(order_by)

        objects1 = objects.filter(**{query_field: q})
        if q.isdigit():
            objects = objects1 | objects.filter(pk=q)
        else:
            objects = objects1

        ret["objects"] = [{
            "id": obj.id,
            "label": "%s - %s" % (obj.pk, unicode(obj)),
            } for obj in objects]

    return HttpResponse("%s(%s)" % (callback, json.dumps(ret)))

