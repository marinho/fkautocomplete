from django.db import models
from django.utils.timezone import now


class Entrada(models.Model):
    user = models.ForeignKey("auth.User")
    criacao = models.DateTimeField(default=now)

