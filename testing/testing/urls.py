from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'myapp.views.home', name='home'),
    url(r'^(\d+)/$', 'myapp.views.home', name='home_edit'),
    url(r'^fk-search$', 'myapp.views.fk_search', name='fk_search'),
    (r'^selectable/', include('selectable.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
